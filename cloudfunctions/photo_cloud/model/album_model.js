// +----------------------------------------------------------------------
// | CCMiniCloud [ Cloud Framework ]
// +----------------------------------------------------------------------
// | Copyright (c) 2021 www.code942.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 明章科技
// +----------------------------------------------------------------------
 
/**
 * Notes: 相册实体
 * Ver : CCMiniCloud Framework 2.0.1 ALL RIGHTS RESERVED BY www.code942.com
 * Date: 2020-10-24 19:20:00
 * Version : CCMiniCloud Framework Ver 2.0.1 ALL RIGHTS RESERVED BY 明章科技
 */


const BaseCCMiniModel = require('./base_ccmini_model.js');

class AlbumModel extends BaseCCMiniModel {

}

// 集合名
AlbumModel.CL = "album";

AlbumModel.CCMINI_DB_STRUCTURE = {
	ALBUM_ID: 'string|true',
	ALBUM_USER_ID: 'string|true',

	ALBUM_TITLE: 'string|true|comment=标题',
	ALBUM_CONTENT: 'string|true|comment=',
	ALBUM_DESC: 'string|false|comment=',
	ALBUM_STATUS: 'int|true|default=1|comment=',
	ALBUM_TYPE: 'string|true|default=其他|comment=他',
	ALBUM_ORDER: 'int|true|default=9999',

	ALBUM_VIEW_CNT: 'int|true|default=0|comment=', 


	ALBUM_PIC: 'array|false|default=[]|comment=',

	ALBUM_ADD_TIME: 'int|true',
	ALBUM_EDIT_TIME: 'int|true',
	ALBUM_ADD_IP: 'string|false',
	ALBUM_EDIT_IP: 'string|false',
};

// 字段前缀
AlbumModel.CCMINI_FIELD_PREFIX = "ALBUM_";

AlbumModel.STATUS = { 
	UNUSE: 0,
	COMM: 1,
	OVER: 7,
	PEDDING: 8,
	DEL: 9
};

AlbumModel.STATUS_DESC = { 
	UNUSE: '待审核',
	COMM: '正常',
	OVER: '结束',
	PEDDING: '停用',
	DEL: '删除'
};


module.exports = AlbumModel;