// +----------------------------------------------------------------------
// | CCMiniCloud [ Cloud Framework ]
// +----------------------------------------------------------------------
// | Copyright (c) 2021 www.code942.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 明章科技
// +----------------------------------------------------------------------

/**
 * Notes: 相册模块控制器
 * Ver : CCMiniCloud Framework 2.0.1 ALL RIGHTS RESERVED BY www.code942.com
 * Date: 2020-09-05 04:00:00
 * Version : CCMiniCloud Framework Ver 2.0.1 ALL RIGHTS RESERVED BY 明章科技
 */

const BaseCCMiniController = require('./base_ccmini_controller.js');
const AlbumService = require('../service/album_service.js');
const ccminiTimeUtil = require('../framework/utils/ccmini_time_util.js');
const ccminiStrUtil = require('../framework/utils/ccmini_str_util.js');
const ccminiContentCheck = require('../framework/validate/ccmini_content_check.js');

class AlbumController extends BaseCCMiniController {
	async getMyAlbumList() {
		// 数据校验
		let rules = {
			search: 'string|min:1|max:30|name=搜索条件',
			sortType: 'string|name=搜索类型',
			sortVal: 'name=搜索类型值',
			orderBy: 'object|name=排序',
			whereEx: 'object|name=附加查询条件',
			page: 'required|int|default=1',
			size: 'int',
			isTotal: 'bool',
			oldTotal: 'int',
		};

		// 取得数据
		let input = this.ccminiValidateData(rules);

		let service = new AlbumService();
		let result = await service.getMyAlbumList(this._userId, input);

		// 数据格式化
		let list = result.list;
		for (let k in list) {
			list[k].ALBUM_ADD_TIME = ccminiTimeUtil.timestamp2Time(list[k].ALBUM_ADD_TIME, 'Y-M-D');

			// 默认图片
			list[k].ALBUM_PIC = ccminiStrUtil.getArrByKey(list[k].ALBUM_PIC, 'cloudId');

		}
		result.list = list;

		return result;
	}
	async getAlbumList() {

		// 数据校验
		let rules = {
			search: 'string|min:1|max:30|name=搜索条件',
			sortType: 'string|name=搜索类型',
			sortVal: 'name=搜索类型值',
			orderBy: 'object|name=排序',
			whereEx: 'object|name=附加查询条件',
			page: 'required|int|default=1',
			size: 'int',
			isTotal: 'bool',
			oldTotal: 'int',
		};

		// 取得数据
		let input = this.ccminiValidateData(rules);

		let service = new AlbumService();
		let result = await service.getAlbumList(input);

		let monthEnglish = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Spt", "Oct", "Nov", "Dec"];


		// 数据格式化
		let list = result.list;
		for (let k in list) {



			list[k].ALBUM_ADD_TIME = ccminiTimeUtil.timestamp2Time(list[k].ALBUM_ADD_TIME, 'Y-M-D');

			let month = list[k].ALBUM_ADD_TIME.split('-')[1];
			list[k].month = Number(month);
			list[k].month = monthEnglish[month - 1];

			let day = list[k].ALBUM_ADD_TIME.split('-')[2];
			list[k].day = day;

			list[k].ALBUM_EXPIRE_TIME = ccminiTimeUtil.timestamp2Time(list[k].ALBUM_EXPIRE_TIME);

			// 默认图片
			list[k].ALBUM_PIC = ccminiStrUtil.getArrByKey(list[k].ALBUM_PIC, 'cloudId');

		}
		result.list = list;

		return result;

	}

	async insertAlbum() {
		// 数据校验
		let rules = {
			title: 'required|string|min:5|max:50|name=相册标题',
			type: 'required|string|min:2|max:10|name=相册分类',
			content: 'required|string|min:10|max:500|name=简要描述'

		};

		// 取得数据
		let input = this.ccminiValidateData(rules);

		await ccminiContentCheck.checkTextMultiClient(input);

		let service = new AlbumService();
		let result = await service.insertAlbum(this._userId, input);



		return result;

	}

	async getMyAlbumDetail() {
		// 数据校验
		let rules = {
			id: 'required|id',
		};

		// 取得数据
		let input = this.ccminiValidateData(rules);

		let service = new AlbumService();
		return await service.getMyAlbumDetail(this._userId, input.id);

	}

	async editAlbum() {
		// 数据校验
		let rules = {
			id: 'required|id',
			title: 'required|string|min:5|max:50|name=相册标题',
			type: 'required|string|min:2|max:10|name=相册分类',
			content: 'required|string|min:10|max:500|name=简要描述',
			desc: 'required|string|min:10|max:200|name=简介',

		};

		// 取得数据
		let input = this.ccminiValidateData(rules);

		await ccminiContentCheck.checkTextMultiClient(input);

		let service = new AlbumService();
		let result = service.editAlbum(this._userId, input);



		return result;
	}

	async delAlbum() {
		// 数据校验
		let rules = {
			id: 'required|id',
		};

		// 取得数据
		let input = this.ccminiValidateData(rules);

		let service = new AlbumService();
		let result = service.delAlbum(this._userId, input.id);



		return result;
	}

	/**
	 * 浏览相册信息
	 */
	async viewAlbum() {
		// 数据校验
		let rules = {
			id: 'required|id',
		};

		// 取得数据
		let input = this.ccminiValidateData(rules);

		let service = new AlbumService();
		let album = await service.viewAlbum(input.id);

		if (album) {
			// 显示转换 
			album.ALBUM_ADD_TIME = ccminiTimeUtil.timestamp2Time(album.ALBUM_ADD_TIME, 'Y-M-D');
			album.ALBUM_EXPIRE_TIME = ccminiTimeUtil.timestamp2Time(album.ALBUM_EXPIRE_TIME, 'Y-M-D');

			album.ALBUM_PIC = ccminiStrUtil.getArrByKey(album.ALBUM_PIC, 'cloudId');
		}

		return album;
	}

	async updateAlbumPic() {
		// 数据校验
		let rules = {
			albumId: 'required|id',
			imgList: 'array'
		};

		// 取得数据
		let input = this.ccminiValidateData(rules);

		let service = new AlbumService();
		return await service.updateAlbumPic(input);
	}

}

module.exports = AlbumController;